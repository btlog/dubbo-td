package com.tiandee.dubbo.test.pipeline;

import com.tiandee.dubbo.common.pipeline.Handler;

/**
 * @author wangjifa
 * @version 1.0
 * @description
 * @createtime 2023/9/20 5:33 PM
 * @updatetime
 */
public class HandlerA implements Handler<Integer,Integer> {
    @Override
    public Integer process(Integer input) {
        System.out.println("执行.....HandlerA.....");
        return input+1;
    }
}
