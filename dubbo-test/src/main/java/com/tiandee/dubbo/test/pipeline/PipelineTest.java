package com.tiandee.dubbo.test.pipeline;

import com.tiandee.dubbo.common.pipeline.Pipeline;

/**
 * @author wangjifa
 * @version 1.0
 * @description
 * @createtime 2023/9/20 5:32 PM
 * @updatetime
 */
public class PipelineTest {

    public void test() {
        Pipeline<Integer, Integer> pipeline = new Pipeline<>(new HandlerA()).addHandler(new HandlerB()).addHandler(new HandlerC());
        int a=1;
        System.out.println(pipeline.execute(1));

    }
}
