package com.tiandee.dubbo.order.provider;

import com.tiandee.dubbo.api.OrderApi;
import com.tiandee.dubbo.api.dto.CreateOrderRequest;
import com.tiandee.dubbo.api.dto.OrderDTO;
import com.tiandee.dubbo.common.Result;
import com.tiandee.dubbo.order.entity.Order;
import com.tiandee.dubbo.order.service.OrderService;
import org.apache.dubbo.config.annotation.DubboService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author wangjifa
 * @version 1.0
 * @description
 * @createtime 2023/8/30 5:03 PM
 * @updatetime
 */
@DubboService
public class OrderProvider implements OrderApi{

    @Autowired
    private OrderService orderService;

    @Override
    public Result<OrderDTO> create(CreateOrderRequest createOrderRequest) {
        Order order = orderService.create(createOrderRequest.getUserId(),createOrderRequest.getCommodityCode(),createOrderRequest.getCount());
        Result<OrderDTO> result = new Result();
        OrderDTO orderDTO = new OrderDTO();
        BeanUtils.copyProperties(order,orderDTO);
        result.setData(orderDTO);
        return result;
    }
}
