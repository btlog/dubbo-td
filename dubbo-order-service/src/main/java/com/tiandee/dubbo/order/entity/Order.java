package com.tiandee.dubbo.order.entity;

import java.io.Serializable;

/**
 * @author wangjifa
 * @version 1.0
 * @description
 * @createtime 2023/8/30 4:49 PM
 * @updatetime
 */
public class Order implements Serializable {

    private static final long serialVersionUID = 5049942021179377715L;

    private Long id;
    private String userId;
    private String commodityCode;
    private Integer count;
    private Integer money;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getCommodityCode() {
        return commodityCode;
    }

    public void setCommodityCode(String commodityCode) {
        this.commodityCode = commodityCode;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public Integer getMoney() {
        return money;
    }

    public void setMoney(Integer money) {
        this.money = money;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
