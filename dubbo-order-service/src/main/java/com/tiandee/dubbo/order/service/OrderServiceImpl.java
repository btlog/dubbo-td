package com.tiandee.dubbo.order.service;


import com.alibaba.nacos.api.config.annotation.NacosValue;
import com.tiandee.dubbo.order.entity.Order;
import org.springframework.stereotype.Service;

/**
 * @author wangjifa
 * @version 1.0
 * @description
 * @createtime 2023/8/30 4:47 PM
 * @updatetime
 */
@Service
public class OrderServiceImpl implements OrderService {


    @NacosValue(value = "${id}", autoRefreshed = true)
    private Long id;

    @Override
    public Order create(String userId, String commodityCode, int orderCount) {
        int orderMoney = calculate(commodityCode, orderCount);

        Order order = new Order();
        order.setUserId(userId);
        order.setCommodityCode(commodityCode);
        order.setCount(orderCount);
        order.setMoney(orderMoney);
        order.setId(id);
        return order;
    }

    private int calculate(String commodityCode, int orderCount) {
        return orderCount*23;
    }

}
