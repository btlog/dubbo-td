package com.tiandee.dubbo.order.service;


import com.tiandee.dubbo.order.entity.Order;

/**
 * @author wangjifa
 * @version 1.0
 * @description
 * @createtime 2023/8/30 4:40 PM
 * @updatetime
 */
public interface OrderService {
    /**
     * 创建订单
     */
    Order create(String userId, String commodityCode, int orderCount);
}
