package com.tiandee.dubbo.order.provider;

import com.tiandee.dubbo.api.AsyncSayHelloApi;
import com.tiandee.dubbo.api.dto.SayHelloRequest;
import com.tiandee.dubbo.common.Result;
import org.apache.dubbo.config.annotation.DubboService;

/**
 * @author wangjifa
 * @version 1.0
 * @description
 * @createtime 2023/9/4 5:18 PM
 * @updatetime
 */
@DubboService(async = true)
public class AsyncSayHelloProvider implements AsyncSayHelloApi {

    @Override
    public Result<String> sayHello(SayHelloRequest sayHelloRequest) {
        System.out.println("hello "+sayHelloRequest.getName());
        Result<String> result = new Result<String>();
        result.setData("异步人");
//        final AsyncContext asyncContext = RpcContext.startAsync();
//        new Thread(() -> {
//            asyncContext.signalContextSwitch();
//            final Result<String> result2 = result;
//            result2.setData("李达叔叔");
//            System.out.println("跟上。。。");
//            asyncContext.write(result);
//        }).start();
//        System.out.println("先走一步");
        return result;
    }

}
