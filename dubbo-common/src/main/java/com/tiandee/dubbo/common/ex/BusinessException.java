package com.tiandee.dubbo.common.ex;

import com.tiandee.dubbo.common.enums.ExceptionEnums;
import com.tiandee.dubbo.common.utils.Strings;

/**
 * @author wangjifa
 * @version 1.0
 * @description
 * @createtime 2023/9/1 9:17 AM
 * @updatetime
 */
public class BusinessException extends RuntimeException{

    private String code;
    private String message;
    private String errorDetail;


    public BusinessException(String code,String message,String errorDetail){
        super(errorDetail);
        this.code = code;
        this.message = message;
        this.errorDetail = errorDetail;
    }


    public BusinessException(ExceptionEnums exceptionEnums,String errorDetail){
        super(errorDetail);
        this.code = exceptionEnums.code();
        this.message = exceptionEnums.message();
        this.errorDetail = errorDetail;
    }

    public BusinessException(ExceptionEnums exceptionEnums){
        super(exceptionEnums.message());
        this.code = exceptionEnums.code();
        this.message = exceptionEnums.message();
    }

    public BusinessException(String code,String message){
        super(message);
        this.code = code;
        this.message = message;
    }

    public BusinessException(String message){
        super(message);
        this.code = ExceptionEnums.ERROR.code();
        this.message = message;
    }


    public String errorDetail() {
        return errorDetail;
    }

    public void setErrorDetail(String errorDetail) {
        this.errorDetail = errorDetail;
    }


    public String code() {
        return code;
    }

    public String message() {
        return message;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(this.code).append(":").append(this.message);
        if (Strings.isNoneBlank(this.errorDetail)) {
            sb.append(":").append(this.errorDetail);
        }
        return sb.toString();
    }


}
