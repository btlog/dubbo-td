package com.tiandee.dubbo.common.core;

import org.apache.dubbo.rpc.model.ApplicationModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextClosedEvent;

public class DubboShutdownApplicationListener implements ApplicationListener<ContextClosedEvent> {

    private static final Logger logger = LoggerFactory.getLogger(DubboShutdownApplicationListener.class);

    @Override
    public void onApplicationEvent(ContextClosedEvent event) {
        logger.info("关闭dubbo");
        ApplicationModel.defaultModel().destroy();
    }
}
