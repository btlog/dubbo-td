package com.tiandee.dubbo.common;

import java.io.Serializable;

/**
 * @author wangjifa
 * @version 1.0
 * @description
 * @createtime 2023/9/1 8:47 AM
 * @updatetime
 */
public class BaseRequest extends BaseInfo implements Serializable{


    private static final long serialVersionUID = 2923152305648359017L;

}
