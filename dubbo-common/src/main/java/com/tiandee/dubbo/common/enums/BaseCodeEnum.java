package com.tiandee.dubbo.common.enums;

/**
 * @author wangjifa
 * @version 1.0
 * @description
 * @createtime 2023/9/1 9:08 AM
 * @updatetime
 */
public interface BaseCodeEnum {
    String code();
    String message();
}
