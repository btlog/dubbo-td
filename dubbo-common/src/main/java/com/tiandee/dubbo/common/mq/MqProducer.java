package com.tiandee.dubbo.common.mq;

import com.tiandee.dubbo.common.Constant;
import com.tiandee.dubbo.common.boot.ApplicationContextHolder;
import com.tiandee.dubbo.common.enums.ExceptionEnums;
import com.tiandee.dubbo.common.ex.MqException;
import com.tiandee.dubbo.common.mq.event.BaseEvent;
import com.tiandee.dubbo.common.utils.Gid;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.rocketmq.client.exception.MQClientException;
import org.apache.rocketmq.client.producer.DefaultMQProducer;
import org.apache.rocketmq.client.producer.SendCallback;
import org.apache.rocketmq.client.producer.SendResult;
import org.apache.rocketmq.client.producer.TransactionMQProducer;
import org.apache.rocketmq.client.producer.TransactionSendResult;
import org.apache.rocketmq.spring.autoconfigure.RocketMQProperties;
import org.apache.rocketmq.spring.core.RocketMQTemplate;
import org.apache.rocketmq.spring.support.RocketMQHeaders;
import org.apache.rocketmq.spring.support.RocketMQMessageConverter;
import org.apache.rocketmq.spring.support.RocketMQUtil;
import org.slf4j.MDC;
import org.springframework.beans.factory.support.BeanDefinitionValidationException;
import org.springframework.context.support.GenericApplicationContext;
import org.springframework.core.env.ConfigurableEnvironment;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.ThreadPoolExecutor;

/**
 * @author wangjifa
 * @version 1.0
 * @description
 * @createtime 2023/9/7 11:21 AM
 * @updatetime
 */
@Slf4j
public class MqProducer {

    /**默认template*/
    private RocketMQTemplate template;
    private RocketMQProperties rocketMQProperties;
    private RocketMQMessageConverter rocketMQMessageConverter;
    private ConfigurableEnvironment environment;
    private ConcurrentHashMap<String,RocketMQTemplate> extTemplateMap = new ConcurrentHashMap();


    public MqProducer(RocketMQTemplate template,RocketMQProperties rocketMQProperties,
                      RocketMQMessageConverter rocketMQMessageConverter,ConfigurableEnvironment environment){
        this.template = template;
        this.rocketMQProperties = rocketMQProperties;
        this.rocketMQMessageConverter = rocketMQMessageConverter;
        this.environment = environment;
    }
    /**获取默认MQTemplate*/
    public RocketMQTemplate getTemplate() {
        return template;
    }
    /**获取扩展MQTemplate*/
    public RocketMQTemplate getExtTemplate(ExtRocketMQTemplateInfo templateInfo){
        RocketMQTemplate template = extTemplateMap.get(templateInfo.beanName());
        if(template == null){
            template = buildMqTemplate(templateInfo);
        }
        return template;
    }

    public <T extends BaseEvent> MqResult execute(SendMessage<T> sendMessage){
        before(sendMessage);
        MqResult mqResult = this.execute(sendMessage.getMessage(),sendMessage.getSendMode(),sendMessage.getDelayLevel(),sendMessage.getSendCallback(),sendMessage.getTemplateInfo());
        after(sendMessage,mqResult);
        return mqResult;
    }

    private <T extends BaseEvent> void before(SendMessage<T> sendMessage) {
        if(StringUtils.isBlank(sendMessage.getMessage().getGid())){
            String gid = MDC.get(Constant.GID);
            sendMessage.getMessage().setGid(StringUtils.isBlank(gid)? Gid.gid():gid);
        }
    }

    private <T extends BaseEvent> void after(SendMessage<T> sendMessage, MqResult mqResult) {
        log.info("[{}]发送[{}]消息[{}]发送结果[{}]", buildDestination(sendMessage.getMessage().getTopicName(),
                sendMessage.getMessage().getBusinessTagId()),sendMessage.getSendMode().message(), sendMessage.getMessage(), mqResult);
    }

    private <T extends BaseEvent> MqResult execute(T message,SendMode sendMode,int delayLevel,SendCallback sendCallback,ExtRocketMQTemplateInfo templateInfo){

        SendResult sendResult = new SendResult();
        MqResult.MqResultBuilder mqResult = MqResult.builder();
        switch (sendMode){
            case SYNC: {
                sendResult = send(message);
                break;
            }
            case ASYNC:{
                asyncSend(message,sendCallback);
                break;
            }
            case ONEWAY: {
                sendOneWay(message);
                break;
            }
            case SYNC_ORDERLY: {
                sendResult = sendOrderly(message);
                break;
            }
            case ASYNC_ORDERLY: {
                asyncSendOrderly(message,sendCallback);
                break;
            }
            case ONEWAY_ORDERLY: {
                sendOneWayOrderly(message);
                break;
            }
            case SYNC_DELAY: {
                sendResult = sendDelay(message,delayLevel);
                break;
            }
            case ASYNC_DELAY: {
                asyncSendDelay(message,sendCallback,delayLevel);
                break;
            }
            case TX: {
                TransactionSendResult result = sendMessageInTransaction(message,templateInfo);
                sendResult = result;
                mqResult.localTransactionState(result.getLocalTransactionState());
                break;
            }
            default: throw new MqException("SendMode 不支持 :"+sendMode.message());
        }
        mqResult.sendResult(sendResult);

        return mqResult.build();

    }


    /**
     * 构建目的地
     */
    public String buildDestination(String topic, String tag) {
        if(StringUtils.isBlank(topic) || StringUtils.isBlank(tag)){
            throw new MqException(ExceptionEnums.PARAMS_ERROR);
        }
        return topic + Constant.DELIMITER + tag;
    }

    private <T extends BaseEvent> Message buildMessage(T message){
        Message<T> sendMessage = MessageBuilder.withPayload(message)
                .setHeader(RocketMQHeaders.KEYS, message.getBusinessKeyId())
                .build();
        return sendMessage;
    }

    private <T extends BaseEvent> SendResult send(T message) {
        SendResult sendResult = template.syncSend(buildDestination(message.getTopicName(),
                message.getBusinessTagId()), buildMessage(message));
        return sendResult;
    }
    private <T extends BaseEvent> SendResult sendDelay(T message, int delayLevel) {
        SendResult sendResult = template.syncSend(buildDestination(message.getTopicName(),
                message.getBusinessTagId()), buildMessage(message), 3000, delayLevel);
        return sendResult;
    }
    private <T extends BaseEvent> SendResult sendOrderly(T message) {
        SendResult sendResult = template.syncSendOrderly(buildDestination(message.getTopicName(),
                message.getBusinessTagId()), buildMessage(message),message.getBusinessKeyId());
        return sendResult;
    }


    private <T extends BaseEvent> void asyncSend(T message, SendCallback sendCallback) {
        String destination = buildDestination(message.getTopicName(),message.getBusinessTagId());
        template.asyncSend(destination, buildMessage(message),sendCallback(destination,message,sendCallback));
    }
    private <T extends BaseEvent> void asyncSendDelay(T message,SendCallback sendCallback, int delayLevel) {
        String destination = buildDestination(message.getTopicName(),message.getBusinessTagId());
        template.asyncSend(destination, buildMessage(message),sendCallback(destination,message,sendCallback), 3000, delayLevel);
    }
    private <T extends BaseEvent> void asyncSendOrderly(T message,SendCallback sendCallback) {
        String destination = buildDestination(message.getTopicName(),message.getBusinessTagId());
        template.asyncSendOrderly(buildDestination(message.getTopicName(),message.getBusinessTagId()),
                buildMessage(message),message.getBusinessKeyId(),sendCallback(destination,message,sendCallback));
    }

    private <T extends BaseEvent> void sendOneWay(T message) {
        template.sendOneWay(buildDestination(message.getTopicName(),message.getBusinessTagId()), buildMessage(message));
    }

    private <T extends BaseEvent> void sendOneWayOrderly(T message) {
        template.sendOneWayOrderly(buildDestination(message.getTopicName(),message.getBusinessTagId()), buildMessage(message), message.getBusinessKeyId());
    }


    private <T extends BaseEvent> SendCallback sendCallback(String destination, T message,SendCallback sendCallback) {
        return sendCallback !=null ? sendCallback : new SendCallback() {

            @Override
            public void onSuccess(SendResult sendResult) {
                log.info("[{}]异步消息成功[{}]", destination, message);
            }

            @Override
            public void onException(Throwable throwable) {
                log.error(String.format("[%s]异步消息出错[%s]发送结果[异常]", destination,message.toString()), throwable);
            }
        };
    }

    private <T extends BaseEvent> TransactionSendResult sendMessageInTransaction(T message, ExtRocketMQTemplateInfo templateInfo) {
        if(templateInfo == null){
            throw new MqException(ExceptionEnums.PARAMS_ERROR,"ExtRocketMQTemplateInfo 不能为空");
        }
        return getExtTemplate(templateInfo).sendMessageInTransaction(buildDestination(message.getTopicName(),
                message.getBusinessTagId()),buildMessage(message),null);
    }


    private synchronized RocketMQTemplate buildMqTemplate(ExtRocketMQTemplateInfo templateInfo){
        RocketMQTemplate template = extTemplateMap.get(templateInfo.beanName());
        if(template!=null){
            return template;
        }
        validate(templateInfo);

        DefaultMQProducer mqProducer = createProducer(templateInfo);
        try {
            mqProducer.start();
        } catch (MQClientException e) {
            throw new BeanDefinitionValidationException(String.format("Failed to startup MQProducer for RocketMQTemplate {}", templateInfo.beanName()), e);
        }
        RocketMQTemplate rocketMQTemplate = new RocketMQTemplate();
        rocketMQTemplate.setProducer(mqProducer);
        rocketMQTemplate.setMessageConverter(rocketMQMessageConverter.getMessageConverter());
        registerTransactionListener(rocketMQTemplate,templateInfo);
        log.info("Set real producer to :{}", templateInfo.beanName());
        extTemplateMap.put(templateInfo.beanName(),rocketMQTemplate);
        return rocketMQTemplate;
    }

    private void validate(ExtRocketMQTemplateInfo templateInfo) {
        if(StringUtils.isBlank(templateInfo.getBeanName())){
            throw new MqException(ExceptionEnums.PARAMS_ERROR,"ExtRocketMQTemplateInfo:benaName不能为空");
        }
        if(StringUtils.isBlank(templateInfo.getGroup())){
            throw new MqException(ExceptionEnums.PARAMS_ERROR,"ExtRocketMQTemplateInfo:group不能为空");
        }

        GenericApplicationContext genericApplicationContext = (GenericApplicationContext) ApplicationContextHolder.get();
        if (genericApplicationContext.isBeanNameInUse(templateInfo.getBeanName())) {
            throw new BeanDefinitionValidationException(String.format("Bean {} has been used in Spring Application Context",templateInfo.getBeanName()));
        }
    }

    private DefaultMQProducer createProducer(ExtRocketMQTemplateInfo annotation) {

        RocketMQProperties.Producer producerConfig = rocketMQProperties.getProducer();
        if (producerConfig == null) {
            producerConfig = new RocketMQProperties.Producer();
        }
        String nameServer = environment.resolvePlaceholders(annotation.nameServer());
        String groupName = environment.resolvePlaceholders(annotation.group());
        groupName = !org.springframework.util.StringUtils.hasLength(groupName) ? producerConfig.getGroup() : groupName;

        String ak = environment.resolvePlaceholders(annotation.accessKey());
        ak = org.springframework.util.StringUtils.hasLength(ak) ? ak : producerConfig.getAccessKey();
        String sk = environment.resolvePlaceholders(annotation.secretKey());
        sk = org.springframework.util.StringUtils.hasLength(sk) ? sk : producerConfig.getSecretKey();
        boolean isEnableMsgTrace = annotation.enableMsgTrace();
        String customizedTraceTopic = environment.resolvePlaceholders(annotation.customizedTraceTopic());
        customizedTraceTopic = org.springframework.util.StringUtils.hasLength(customizedTraceTopic) ? customizedTraceTopic : producerConfig.getCustomizedTraceTopic();
        //if String is not is equal "true" TLS mode will represent the as default value false
        boolean useTLS = new Boolean(environment.resolvePlaceholders(annotation.tlsEnable()));

        DefaultMQProducer producer = RocketMQUtil.createDefaultMQProducer(groupName, ak, sk, isEnableMsgTrace, customizedTraceTopic);

        producer.setNamesrvAddr(nameServer);
        producer.setSendMsgTimeout(annotation.sendMessageTimeout() == -1 ? producerConfig.getSendMessageTimeout() : annotation.sendMessageTimeout());
        producer.setRetryTimesWhenSendFailed(annotation.retryTimesWhenSendFailed() == -1 ? producerConfig.getRetryTimesWhenSendFailed() : annotation.retryTimesWhenSendFailed());
        producer.setRetryTimesWhenSendAsyncFailed(annotation.retryTimesWhenSendAsyncFailed() == -1 ? producerConfig.getRetryTimesWhenSendAsyncFailed() : annotation.retryTimesWhenSendAsyncFailed());
        producer.setMaxMessageSize(annotation.maxMessageSize() == -1 ? producerConfig.getMaxMessageSize() : annotation.maxMessageSize());
        producer.setCompressMsgBodyOverHowmuch(annotation.compressMessageBodyThreshold() == -1 ? producerConfig.getCompressMessageBodyThreshold() : annotation.compressMessageBodyThreshold());
        producer.setRetryAnotherBrokerWhenNotStoreOK(annotation.retryNextServer());
        producer.setUseTLS(useTLS);
        String namespace = environment.resolvePlaceholders(annotation.namespace());
        producer.setNamespace(RocketMQUtil.getNamespace(namespace, producerConfig.getNamespace()));
        producer.setInstanceName(annotation.instanceName());

        return producer;
    }

    private void registerTransactionListener(RocketMQTemplate rocketMQTemplate,ExtRocketMQTemplateInfo templateInfo) {
        if(templateInfo.getRocketMQLocalTransactionListener() == null){
            throw new MqException(ExceptionEnums.PARAMS_ERROR,"ExtRocketMQTemplateInfo:rocketMQLocalTransactionListener 不能为空");
        }
        ((TransactionMQProducer) rocketMQTemplate.getProducer()).setExecutorService(new ThreadPoolExecutor(templateInfo.getCorePoolSize(), templateInfo.getMaximumPoolSize(),
                templateInfo.getKeepAliveTime(), templateInfo.getKeepAliveTimeUnit(), new LinkedBlockingDeque<>(templateInfo.getBlockingQueueSize())));

        ((TransactionMQProducer) rocketMQTemplate.getProducer()).setTransactionListener(RocketMQUtil.convert(templateInfo.getRocketMQLocalTransactionListener()));

        log.debug("RocketMQLocalTransactionListener  register to {} success", templateInfo.getBeanName());
    }





}
