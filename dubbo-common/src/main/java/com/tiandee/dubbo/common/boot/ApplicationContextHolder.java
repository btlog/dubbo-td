package com.tiandee.dubbo.common.boot;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.ConfigurableApplicationContext;

/**
 * @author wangjifa
 * @version 1.0
 * @description
 * @createtime 2023/9/8 10:49 AM
 * @updatetime
 */
public class ApplicationContextHolder implements ApplicationContextAware {

    private static ApplicationContext CONTEXT;

    private static void setContext(ApplicationContext context) {
        ApplicationContextHolder.CONTEXT = context;
    }

    public static ConfigurableApplicationContext get() {
        return (ConfigurableApplicationContext) CONTEXT;
    }

    @Override
    public void setApplicationContext(ApplicationContext context) throws BeansException {
        if (CONTEXT != null) {
            if (context.getParent() == CONTEXT) {
                setContext(context);
            }
        } else {
            setContext(context);
        }
    }
}
