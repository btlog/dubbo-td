package com.tiandee.dubbo.common.core;


import com.google.common.base.Strings;
import com.tiandee.dubbo.common.BaseRequest;
import org.apache.dubbo.common.extension.Activate;
import org.apache.dubbo.rpc.Filter;
import org.apache.dubbo.rpc.Invocation;
import org.apache.dubbo.rpc.Invoker;
import org.apache.dubbo.rpc.Result;
import org.apache.dubbo.rpc.RpcContext;
import org.apache.dubbo.rpc.RpcException;

import static org.apache.dubbo.common.constants.CommonConstants.CONSUMER;
import static org.apache.dubbo.common.constants.CommonConstants.PROVIDER;


@Activate(group = {PROVIDER, CONSUMER})
public class RequestContextFilter implements Filter {
    @Override
    public Result invoke(Invoker<?> invoker, Invocation inv) throws RpcException {
        if (RpcContext.getServerContext().isProviderSide()) {
            try {
                Object[] args = inv.getArguments();
                if (args != null) {
                    for (Object arg : args) {
                        if (arg instanceof BaseRequest) {
                            RequestContext ctx = RequestContext.getContext();
                            ctx.setGid(((BaseRequest) arg).getGid());
                            break;
                        }
                    }
                }
                return invoker.invoke(inv);
            } finally {
                RequestContext.removeContext();
            }
        } else {
            Object[] args = inv.getArguments();
            if (args != null) {
                for (Object arg : args) {
                    if (arg instanceof BaseRequest) {
                        RequestContext ctx = RequestContext.getContext();
                        if (Strings.isNullOrEmpty(((BaseRequest) arg).getGid())) {
                            ((BaseRequest) arg).setGid(ctx.getGid());
                        }
                        break;
                    }
                }
            }
            return invoker.invoke(inv);
        }
    }
}
