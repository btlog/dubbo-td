package com.tiandee.dubbo.common.core;

import com.tiandee.dubbo.common.BaseRequest;
import com.tiandee.dubbo.common.Constant;
import org.apache.dubbo.common.extension.Activate;
import org.apache.dubbo.rpc.Filter;
import org.apache.dubbo.rpc.Invocation;
import org.apache.dubbo.rpc.Invoker;
import org.apache.dubbo.rpc.Result;
import org.apache.dubbo.rpc.RpcException;
import org.slf4j.MDC;

import static org.apache.dubbo.common.constants.CommonConstants.PROVIDER;


@Activate(
        group = {PROVIDER},
        order = Integer.MIN_VALUE
)
public class MDCFilter implements Filter {

    @Override
    public Result invoke(Invoker<?> invoker, Invocation inv) throws RpcException {
        Object[] args = inv.getArguments();
        boolean mdcEnable = false;
        if (args != null) {
            for (Object arg : args) {
                if (arg instanceof BaseRequest) {
                    mdcEnable = true;
                    setGid((BaseRequest) arg);
                    break;
                }
            }
        }
        Result result;
        try {
            result = invoker.invoke(inv);
        } finally {
            if (mdcEnable) {
                MDC.remove(Constant.GID);
            }
        }
        return result;
    }

    private void setGid(BaseRequest arg) {
        String gid = arg.getGid();
        if (gid == null) {
            gid = "";
        }
        MDC.put(Constant.GID, gid);
    }
}
