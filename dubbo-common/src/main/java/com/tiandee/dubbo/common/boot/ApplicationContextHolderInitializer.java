package com.tiandee.dubbo.common.boot;

import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.core.Ordered;
import org.springframework.core.PriorityOrdered;

/**
 * @author wangjifa
 * @version 1.0
 * @description
 * @createtime 2023/9/8 10:55 AM
 * @updatetime
 */
public class ApplicationContextHolderInitializer  implements ApplicationContextInitializer<ConfigurableApplicationContext>, PriorityOrdered {
    @Override
    public void initialize(ConfigurableApplicationContext applicationContext) {
        new ApplicationContextHolder().setApplicationContext(applicationContext);
    }

    @Override
    public int getOrder() {
        return Ordered.HIGHEST_PRECEDENCE;
    }
}