package com.tiandee.dubbo.common.mq;


import lombok.Getter;
import lombok.Setter;
import org.apache.rocketmq.spring.core.RocketMQLocalTransactionListener;

import java.util.concurrent.TimeUnit;

/**
 * @author wangjifa
 * @version 1.0
 * @description
 * @createtime 2023/9/8 11:27 AM
 * @updatetime
 */
@Setter
@Getter
public class ExtRocketMQTemplateInfo {

    /**扩展MQtemplate名称，唯一*/
    private String beanName;

    /**producter 参数*/
    private String nameServer = "${rocketmq.name-server:}";
    private String group = "${rocketmq.producer.group:}";
    private int sendMessageTimeout=-1;
    private int compressMessageBodyThreshold=-1;
    private int retryTimesWhenSendFailed=-1;
    private int retryTimesWhenSendAsyncFailed=-1;
    private boolean retryNextServer=false;
    private int maxMessageSize=-1;
    private String accessKey="${rocketmq.producer.accessKey:}";
    private String secretKey="${rocketmq.producer.secretKey:}";
    private boolean enableMsgTrace=false;
    private String customizedTraceTopic="${rocketmq.producer.customized-trace-topic:}";
    private String tlsEnable="false";
    private String namespace="";
    private String instanceName="DEFAULT";


    /**MQtemplate 参数*/
    private int corePoolSize = 1;
    private int maximumPoolSize = 1;
    private long keepAliveTime = 1000 * 60;
    private TimeUnit keepAliveTimeUnit = TimeUnit.MILLISECONDS;
    private int blockingQueueSize = 2000;

    /**事务MQ*/
    private RocketMQLocalTransactionListener rocketMQLocalTransactionListener;



    public String beanName() {
        return beanName;
    }

    public String nameServer() {
        return nameServer;
    }

    public String group() {
        return group;
    }

    public int sendMessageTimeout() {
        return sendMessageTimeout;
    }

    public int compressMessageBodyThreshold() {
        return compressMessageBodyThreshold;
    }

    public int retryTimesWhenSendFailed() {
        return retryTimesWhenSendFailed;
    }

    public int retryTimesWhenSendAsyncFailed() {
        return retryTimesWhenSendAsyncFailed;
    }

    public boolean retryNextServer() {
        return retryNextServer;
    }

    public int maxMessageSize() {
        return maxMessageSize;
    }

    public String accessKey() {
        return accessKey;
    }

    public String secretKey() {
        return secretKey;
    }

    public boolean enableMsgTrace() {
        return enableMsgTrace;
    }

    public String customizedTraceTopic() {
        return customizedTraceTopic;
    }

    public String tlsEnable() {
        return tlsEnable;
    }

    public String namespace() {
        return namespace;
    }

    public String instanceName() {
        return instanceName;
    }

}
