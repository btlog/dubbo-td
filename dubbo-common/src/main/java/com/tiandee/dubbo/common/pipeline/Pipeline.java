package com.tiandee.dubbo.common.pipeline;

/**
 * @author wangjifa
 * @version 1.0
 * @description
 * @createtime 2023/9/20 5:25 PM
 * @updatetime
 */
public class Pipeline<I,O> {

    private final Handler<I,O> currentHandler;

    public Pipeline(Handler<I, O> currentHandler) {
        this.currentHandler = currentHandler;
    }



    public <K> Pipeline<I,K> addHandler(Handler<O, K> newHandler){
        return new Pipeline<>(input -> newHandler.process(currentHandler.process(input)));
    }

    public O execute(I input) {
        return currentHandler.process(input);
    }
}
