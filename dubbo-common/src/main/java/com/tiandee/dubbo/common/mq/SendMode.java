package com.tiandee.dubbo.common.mq;

import com.tiandee.dubbo.common.enums.BaseCodeEnum;

/**
 * @author wangjifa
 * @version 1.0
 * @description
 * @createtime 2023/9/7 2:50 PM
 * @updatetime
 */
public enum SendMode implements BaseCodeEnum {
    SYNC("SYNC","同步"),
    ASYNC("ASYNC","异步"),
    ONEWAY("ONEWAY","单向"),
    SYNC_ORDERLY("SYNC_ORDERLY","顺序同步"),
    ASYNC_ORDERLY("ASYNC_ORDERLY","顺序异步"),
    ONEWAY_ORDERLY("ONEWAY_ORDERLY","顺序单向"),
    SYNC_DELAY("SYNC_DELAY","延迟同步"),
    ASYNC_DELAY("ASYNC_DELAY","延迟异步"),
    TX("TX","事务"),
    ;

    private String code;
    private String message;

    SendMode(String code,String message){
        this.code = code;
        this.message = message;
    }
    @Override
    public String code() {
        return code;
    }

    @Override
    public String message() {
        return message;
    }
}
