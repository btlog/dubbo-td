package com.tiandee.dubbo.common.core;

import org.apache.dubbo.common.threadpool.ThreadPool;
import org.apache.dubbo.common.utils.NamedThreadFactory;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.Executor;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import static org.apache.dubbo.common.constants.CommonConstants.DEFAULT_QUEUES;
import static org.apache.dubbo.common.constants.CommonConstants.DEFAULT_THREAD_NAME;
import static org.apache.dubbo.common.constants.CommonConstants.QUEUES_KEY;
import static org.apache.dubbo.common.constants.CommonConstants.THREADS_KEY;
import static org.apache.dubbo.common.constants.CommonConstants.THREAD_NAME_KEY;

public class ExDubboThreadPool implements ThreadPool {

    @Override
    public Executor getExecutor(org.apache.dubbo.common.URL url) {
        //使用固定线程池
        String name = url.getParameter(THREAD_NAME_KEY, DEFAULT_THREAD_NAME);
        int threads = url.getParameter(THREADS_KEY, Integer.MAX_VALUE);
        int queues = url.getParameter(QUEUES_KEY,DEFAULT_QUEUES);
        ThreadPoolExecutor threadPoolExecutor =
                new ThreadPoolExecutor(
                        threads,
                        threads,
                        0,
                        TimeUnit.MILLISECONDS,
                        queues == 0
                                ? new SynchronousQueue<>()
                                : (queues < 0 ? new LinkedBlockingQueue<>() : new ArrayBlockingQueue<>(queues)),
                        new NamedThreadFactory(name, true));
        return threadPoolExecutor;
    }

}
