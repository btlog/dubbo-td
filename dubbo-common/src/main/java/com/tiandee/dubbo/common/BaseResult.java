package com.tiandee.dubbo.common;

import com.tiandee.dubbo.common.enums.ExceptionEnums;

import java.io.Serializable;

/**
 * @author wangjifa
 * @version 1.0
 * @description
 * @createtime 2023/9/1 8:47 AM
 * @updatetime
 */
public class BaseResult extends BaseInfo implements Serializable{

    private static final long serialVersionUID = -7104735774192760721L;

    private String code = ExceptionEnums.SUCCESS.code();
    private boolean success = true;
    private String message ="请求成功";
    private Long ts = System.currentTimeMillis();

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Long getTs() {
        return ts;
    }

    public void setTs(Long ts) {
        this.ts = ts;
    }


}
