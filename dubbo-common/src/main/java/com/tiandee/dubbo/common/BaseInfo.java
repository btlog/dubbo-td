package com.tiandee.dubbo.common;

import com.tiandee.dubbo.common.utils.ToString;

import java.io.Serializable;

/**
 * @author wangjifa
 * @version 1.0
 * @description
 * @createtime 2023/9/7 10:07 AM
 * @updatetime
 */
public class BaseInfo implements Serializable{
    private static final long serialVersionUID = -5961128778426165273L;

    private String gid;

    public String getGid() {
        return gid;
    }

    public void setGid(String gid) {
        this.gid = gid;
    }

    @Override
    public String toString(){
        return ToString.toString(this);
    }
}
