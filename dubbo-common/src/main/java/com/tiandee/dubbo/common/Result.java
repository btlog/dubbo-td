package com.tiandee.dubbo.common;

import java.io.Serializable;

/**
 * @author wangjifa
 * @version 1.0
 * @description
 * @createtime 2023/8/31 1:47 PM
 * @updatetime
 */
public class Result<T> extends BaseResult implements Serializable{
    private static final long serialVersionUID = -8371363984809488188L;
    

    private T data;

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

}
