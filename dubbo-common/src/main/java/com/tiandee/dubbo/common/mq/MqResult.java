package com.tiandee.dubbo.common.mq;

import com.tiandee.dubbo.common.BaseInfo;
import lombok.Builder;
import lombok.ToString;
import org.apache.rocketmq.client.producer.LocalTransactionState;

/**
 * @author wangjifa
 * @version 1.0
 * @description
 * @createtime 2023/9/8 4:01 PM
 * @updatetime
 */
@Builder
@ToString
public class MqResult extends BaseInfo {
    private LocalTransactionState localTransactionState;
    org.apache.rocketmq.client.producer.SendResult sendResult;
}
