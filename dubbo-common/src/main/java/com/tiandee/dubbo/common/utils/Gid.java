package com.tiandee.dubbo.common.utils;

import java.util.UUID;

/**
 * @author wangjifa
 * @version 1.0
 * @description
 * @createtime 2023/9/7 5:44 PM
 * @updatetime
 */
public class Gid {
    public static String gid(){
        return UUID.randomUUID().toString().replaceAll("-","");
    }
}
