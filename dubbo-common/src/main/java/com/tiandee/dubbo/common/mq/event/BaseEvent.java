package com.tiandee.dubbo.common.mq.event;

import com.tiandee.dubbo.common.BaseInfo;

import java.io.Serializable;

/**
 * @author wangjifa
 * @version 1.0
 * @description
 * @createtime 2023/9/5 6:08 PM
 * @updatetime
 */
public class BaseEvent extends BaseInfo implements Serializable{

    private static final long serialVersionUID = 642365993521683012L;

    private String source = "";
    private String topicName;
    private String businessKeyId;
    private String businessTagId;
    /**发送时间*/
    private long sendMessageTime = System.currentTimeMillis();
    /**
     * 重试次数，用于判断重试次数，超过重试次数发送异常警告
     */
    private Integer retryTimes = 0;


    public String getTopicName() {
        return topicName;
    }

    public void setTopicName(String topicName) {
        this.topicName = topicName;
    }

    public String getBusinessKeyId() {
        return businessKeyId;
    }

    public void setBusinessKeyId(String businessKeyId) {
        this.businessKeyId = businessKeyId;
    }

    public String getBusinessTagId() {
        return businessTagId;
    }

    public void setBusinessTagId(String businessTagId) {
        this.businessTagId = businessTagId;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public Integer getRetryTimes() {
        return retryTimes;
    }

    public void setRetryTimes(Integer retryTimes) {
        this.retryTimes = retryTimes;
    }

    public long getSendMessageTime() {
        return sendMessageTime;
    }

    public void setSendMessageTime(long sendMessageTime) {
        this.sendMessageTime = sendMessageTime;
    }
}
