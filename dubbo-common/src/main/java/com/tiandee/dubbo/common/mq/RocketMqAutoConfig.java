package com.tiandee.dubbo.common.mq;

import org.apache.rocketmq.spring.autoconfigure.RocketMQProperties;
import org.apache.rocketmq.spring.core.RocketMQTemplate;
import org.apache.rocketmq.spring.support.RocketMQMessageConverter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.ConfigurableEnvironment;

/**
 * @author wangjifa
 * @version 1.0
 * @description
 * @createtime 2023/9/7 11:25 AM
 * @updatetime
 */
@Configuration
public class RocketMqAutoConfig {

    @Bean
    public MqProducer mqProducer(RocketMQTemplate rocketMQTemplate, RocketMQProperties rocketMQProperties,
                                 RocketMQMessageConverter rocketMQMessageConverter,ConfigurableEnvironment environment){
        return new MqProducer(rocketMQTemplate,rocketMQProperties,rocketMQMessageConverter,environment);
    }

}
