package com.tiandee.dubbo.common.ex;

import com.tiandee.dubbo.common.enums.ExceptionEnums;

/**
 * @author wangjifa
 * @version 1.0
 * @description
 * @createtime 2023/9/7 2:47 PM
 * @updatetime
 */
public class MqException extends BusinessException{
    public MqException(String code, String message, String errorDetail) {
        super(code, message, errorDetail);
    }

    public MqException(ExceptionEnums exceptionEnums, String errorDetail) {
        super(exceptionEnums, errorDetail);
    }

    public MqException(ExceptionEnums exceptionEnums) {
        super(exceptionEnums);
    }

    public MqException(String code, String message) {
        super(code, message);
    }

    public MqException(String message) {
        super(message);
    }
}
