package com.tiandee.dubbo.common;

import com.tiandee.dubbo.common.utils.ToString;

import java.io.Serializable;

/**
 * @author wangjifa
 * @version 1.0
 * @description
 * @createtime 2023/9/1 5:11 PM
 * @updatetime
 */
public class BaseDTO implements Serializable{
    private static final long serialVersionUID = -8724735831800303879L;
    @Override
    public String toString(){
        return ToString.toString(this);
    }


}
