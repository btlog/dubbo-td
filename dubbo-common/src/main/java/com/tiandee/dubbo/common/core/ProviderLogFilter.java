package com.tiandee.dubbo.common.core;

import com.google.common.collect.Maps;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.rpc.Filter;
import org.apache.dubbo.rpc.Invocation;
import org.apache.dubbo.rpc.Invoker;
import org.apache.dubbo.rpc.Result;
import org.apache.dubbo.rpc.RpcContext;
import org.apache.dubbo.rpc.RpcException;

import java.lang.reflect.Method;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

import static org.apache.dubbo.common.constants.CommonConstants.GROUP_KEY;


@Slf4j
public class ProviderLogFilter implements Filter {

    private static final AtomicLong requestId = new AtomicLong();
    private static String providerLogEnableKey = "dubboProviderLogEnableKey";

    private static Map<String, Method> methodMap = Maps.newHashMap();

    @Override
    public Result invoke(Invoker<?> invoker, Invocation inv) throws RpcException {

        String methodString = formatMethodGroup(invoker, inv);

        if (isIgnore(invoker, inv)) {
            return invoker.invoke(inv);
        } else {
            RpcContext context = RpcContext.getServerContext();
            long id = requestId.getAndIncrement();
            long now = System.currentTimeMillis();
            try {
                context.setAttachment(providerLogEnableKey, Boolean.TRUE);
                StringBuilder sn = new StringBuilder(200);
                sn.append(methodString);
                Object[] args = inv.getArguments();
                if (args != null && args.length > 0) {
                    sn.append(com.tiandee.dubbo.common.utils.ToString.toString(args));
                }
                sn.append(" ip:").append(context.getRemoteHost());
                String msg = sn.toString();
                log.info("[DUBBO-P-{}] 请求:{}", id, msg);
            } catch (Exception t) {
                log.warn("Exception in ProviderLogFilter of service( {} -> {})", invoker, inv, t);
            }

            Result result = invoker.invoke(inv);
            log.info("[DUBBO-P-{}] 响应:{} 耗时:{}ms", id, result.getValue(), System.currentTimeMillis() - now);
            return result;
        }
    }

    public static boolean isIgnore(Invoker<?> invoker, Invocation inv) {
        Method method = getMethodByCache(invoker, inv);
        if (method == null) {
            return false;
        }
        DubboLogIgnore dubboLogIgnore = method.getAnnotation(DubboLogIgnore.class);
        return dubboLogIgnore != null;
    }


    public static String formatMethodGroup(Invoker<?> invoker, Invocation inv) {
        String serviceName = invoker.getInterface().getName();
        String group = invoker.getUrl().getParameter(GROUP_KEY);
        String methodName = inv.getMethodName();
        return formatMethodGroup(serviceName, group, methodName);
    }


    public static String formatMethodGroup(String serviceName, String group, String methodName) {
        String methodString;
        if (group == null || group.length()==0) {
            methodString = String.format("%s#%s", serviceName, methodName);
        } else {
            methodString = String.format("%s/%s#%s", serviceName, group, methodName);
        }
        return methodString;
    }

    private static Method getMethodByCache(Invoker<?> invoker, Invocation inv) {
        Method method = null;
        String methodFullName = formatMethodGroup(invoker, inv);
        if (methodMap.containsKey(methodFullName)) {
            method = methodMap.get(methodFullName);
        } else {
            Class<?> anInterface = invoker.getInterface();
            String methodName = inv.getMethodName();
            try {
                method = anInterface.getMethod(methodName, inv.getParameterTypes());
                methodMap.put(methodFullName, method);
            } catch (NoSuchMethodException e) {
                log.error("no such method {}.{}", anInterface.getName(), methodName, e);
            }
        }
        return method;
    }
}
