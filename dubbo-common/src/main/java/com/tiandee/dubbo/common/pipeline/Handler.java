package com.tiandee.dubbo.common.pipeline;

/**
 * @author wangjifa
 * @version 1.0
 * @description
 * @createtime 2023/9/20 5:24 PM
 * @updatetime
 */
public interface Handler<I,O> {

    O process(I input);
}
