package com.tiandee.dubbo.common.core;

import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class RequestContext {

    private static final ThreadLocal<RequestContext> LOCAL = ThreadLocal.withInitial(() -> new RequestContext());

    private String gid;

    public static RequestContext getContext() {
        return LOCAL.get();
    }

    public static void removeContext() {
        LOCAL.remove();
    }


}
