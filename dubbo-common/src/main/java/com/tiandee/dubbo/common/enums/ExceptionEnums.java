package com.tiandee.dubbo.common.enums;

/**
 * @author wangjifa
 * @version 1.0
 * @description
 * @createtime 2023/9/1 9:11 AM
 * @updatetime
 */
public enum ExceptionEnums implements BaseCodeEnum{

    ERROR("1","失败"),
    SUCCESS("0","成功"),
    PARAMS_ERROR("2","参数异常"),
    ;

    private String code;

    private String message;

    ExceptionEnums(String code, String message) {
        this.code = code;
        this.message = message;
    }


    @Override
    public String code(){
        return code;
    }
    @Override
    public String message(){
        return message;
    }

    public static ExceptionEnums statOf(String code) {
        for (ExceptionEnums state : values()) {
            if (state.code().equals(code)){
                return state;
            }
        }
        return null;
    }

}
