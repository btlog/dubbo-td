package com.tiandee.dubbo.common.core;

import com.tiandee.dubbo.common.utils.ToString;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.rpc.Filter;
import org.apache.dubbo.rpc.Invocation;
import org.apache.dubbo.rpc.Invoker;
import org.apache.dubbo.rpc.Result;
import org.apache.dubbo.rpc.RpcContext;
import org.apache.dubbo.rpc.RpcException;

import java.util.concurrent.atomic.AtomicLong;

import static com.tiandee.dubbo.common.core.ProviderLogFilter.isIgnore;
import static org.apache.dubbo.common.constants.CommonConstants.GROUP_KEY;

/**
 * @author wangjifa
 * @version 1.0
 * @description
 * @createtime 2023/9/1 5:23 PM
 * @updatetime
 */
@Slf4j
public class ConsumerLogFilter implements Filter {

    private static final AtomicLong requestId = new AtomicLong();

    @Override
    public Result invoke(Invoker<?> invoker, Invocation inv) throws RpcException {

        if (isIgnore(invoker, inv)) {
            return invoker.invoke(inv);
        } else {
            long id = requestId.getAndIncrement();
            long now = System.currentTimeMillis();
            try {
                RpcContext context = RpcContext.getServerContext();
                String serviceName = invoker.getInterface().getSimpleName();
                String group = invoker.getUrl().getParameter(GROUP_KEY);
                StringBuilder sn = new StringBuilder(200);
                if (null != group && group.length() > 0) {
                    sn.append(group).append("/");
                }
                sn.append(serviceName);
                sn.append("#");
                sn.append(inv.getMethodName());
                Object[] args = inv.getArguments();
                if (args != null && args.length > 0) {
                    sn.append(ToString.toString(args));
                }
                sn.append(" ip:")
                        .append(context.getRemoteHost())
                        .append(":")
                        .append(context.getRemotePort());
                String msg = sn.toString();
                log.info("[DUBBO-C-{}] 请求:{}", id, msg);
            } catch (Throwable t) {
                log.warn("Exception in ConsumerRequestLogFilter of service(" + invoker + " -> " + inv + ")", t);
            }

            Result result = invoker.invoke(inv);
            log.info("[DUBBO-C-{}] 响应:{} 耗时:{}ms", id, result.getValue(), System.currentTimeMillis() - now);
            return result;
        }
    }
}
