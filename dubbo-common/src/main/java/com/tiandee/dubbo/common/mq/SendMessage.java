package com.tiandee.dubbo.common.mq;

import com.tiandee.dubbo.common.mq.event.BaseEvent;
import lombok.Builder;
import lombok.Getter;
import org.apache.rocketmq.client.producer.SendCallback;

/**
 * @author wangjifa
 * @version 1.0
 * @description
 * @createtime 2023/9/7 3:49 PM
 * @updatetime
 */
@Builder
@Getter
public class SendMessage<T extends BaseEvent>{
    private static final SendMode DEFAULT_SENDMODE =  SendMode.SYNC;
    private static final int DEFAULT_DELAYLEVEL = 3;

    private T message;
    /**默认同步发送消息*/
    @Builder.Default
    private SendMode sendMode = DEFAULT_SENDMODE;
    @Builder.Default
    private int delayLevel = DEFAULT_DELAYLEVEL;
    private SendCallback sendCallback;
    private ExtRocketMQTemplateInfo templateInfo;

}
