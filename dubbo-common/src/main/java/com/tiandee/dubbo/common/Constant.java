package com.tiandee.dubbo.common;

/**
 * @author wangjifa
 * @version 1.0
 * @description
 * @createtime 2023/9/7 11:23 AM
 * @updatetime
 */
public class Constant {
    public static final String DELIMITER = ":";
//    public static final String TRACE_ID = "traceId";
    public static final String GID = "gid";
}
