package com.tiandee.dubbo.common.mq;

import com.tiandee.dubbo.common.Constant;
import com.tiandee.dubbo.common.mq.event.BaseEvent;
import org.apache.rocketmq.client.producer.SendStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;

import javax.annotation.Resource;
import java.time.Duration;
import java.time.Instant;

/**
 * @author wangjifa
 * @version 1.0
 * @description
 * @createtime 2023/9/7 5:22 PM
 * @updatetime
 */
public abstract class MqConsumer<T extends BaseEvent> {
    /**
     * 这里的日志记录器是哪个子类的就会被哪个子类的类进行初始化
     */
    protected final Logger logger = LoggerFactory.getLogger(this.getClass());
    @Resource
    private MqProducer mqProducer;

    /**
     * 消息者名称
     */
    protected abstract String consumerName();

    /**
     * 消息处理
     * @param message 待处理消息
     * @throws Exception 消费异常
     */
    protected abstract void handleMessage(T message) throws Exception;

    /**
     * 超过重试次数消息，需要启用isRetry
     *
     * @param message 待处理消息
     */
    protected abstract void overMaxRetryTimesMessage(T message);
    /**
     * 是否过滤消息，例如某些
     *
     * @param message 待处理消息
     * @return true: 本次消息被过滤，false：不过滤
     */
    protected boolean isFilter(T message) {
        return false;
    }

    /**
     * 是否异常时重复发送
     *
     * @return true: 消息重试，false：不重试
     */
    protected abstract boolean isRetry();

    /**
     * 消费异常时是否抛出异常
     *
     * @return true: 抛出异常，false：消费异常(如果没有开启重试则消息会被自动ack)
     */
    protected abstract boolean isThrowException();

    /**
     * 最大重试次数
     *
     * @return 最大重试次数，默认10次
     */
    protected int maxRetryTimes() {
        return 10;
    }

    /**
     * isRetry开启时，重新入队延迟时间
     * @return -1：立即入队重试
     */
    protected int retryDelayLevel() {
        return -1;
    }


    public void dispatchMessage(T message) {
        MDC.put(Constant.GID, message.getGid());
        logger.info("[{}]消费[{}]", consumerName(), message);
        if (isFilter(message)) {
            logger.info("消息不满足消费条件，已过滤[{}]",message.getGid());
            return;
        }
        // 超过最大重试次数时调用子类方法处理
        if (message.getRetryTimes() > maxRetryTimes()) {
            overMaxRetryTimesMessage(message);
            return;
        }
        try {
            Instant start = Instant.now();
            handleMessage(message);
            logger.info("消费成功[{}]，耗时[{}ms]",message.getGid(),Duration.between(start, Instant.now()).toMillis());
        } catch (Exception e) {
            logger.error(String.format("消费异常[%s],%s",message.getGid(),message), e);
            if (isThrowException()) {
                throw new RuntimeException(e);
            }
            if (isRetry()) {
                message.setSource(message.getSource() + "消息重试");
                message.setRetryTimes(message.getRetryTimes() + 1);
                MqResult mqResult;
                try {
                    mqResult = mqProducer.execute(SendMessage.builder().message(message).sendMode(SendMode.SYNC_DELAY).delayLevel(retryDelayLevel()).build());
                } catch (Exception ex) {
                    throw new RuntimeException(ex);
                }
                // 发送失败的处理就是不进行ACK，由RocketMQ重试
                if (mqResult!=null && mqResult.sendResult!=null && mqResult.sendResult.getSendStatus() != SendStatus.SEND_OK) {
                    throw new RuntimeException("重试消息发送失败");
                }
            }
        }finally {
            MDC.remove(Constant.GID);
        }
    }
}
