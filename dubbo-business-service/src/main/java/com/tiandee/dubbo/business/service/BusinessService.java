package com.tiandee.dubbo.business.service;

import com.tiandee.dubbo.api.dto.OrderDTO;
import com.tiandee.dubbo.business.proxy.OrderProxy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author wangjifa
 * @version 1.0
 * @description
 * @createtime 2023/8/31 2:03 PM
 * @updatetime
 */
@Service
public class BusinessService {

    @Autowired
    private OrderProxy orderProxy;


//    private StorageService storageService;
//
//    private OrderService orderService;

    /**
     * 采购
     */
    public OrderDTO purchase(String userId, String commodityCode, int orderCount) {

        //storageService.deduct(commodityCode, orderCount);

        return orderProxy.createOrder(userId, commodityCode, orderCount);
    }


}
