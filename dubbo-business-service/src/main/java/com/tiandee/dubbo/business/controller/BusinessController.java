package com.tiandee.dubbo.business.controller;

import com.tiandee.dubbo.api.dto.OrderDTO;
import com.tiandee.dubbo.business.service.BusinessService;
import com.tiandee.dubbo.common.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author wangjifa
 * @version 1.0
 * @description
 * @createtime 2023/8/31 1:45 PM
 * @updatetime
 */
@RestController
public class BusinessController {

    @Autowired
    private BusinessService businessService;

    @GetMapping("test")
    public Result test(){
        return new Result();
    }

    @GetMapping("purchase")
//    @PostMapping
    public Result purchase(){
        Result<OrderDTO> result = new Result();
        OrderDTO orderDTO = businessService.purchase("123","321",12);
        result.setData(orderDTO);
        return result;
    }

}
