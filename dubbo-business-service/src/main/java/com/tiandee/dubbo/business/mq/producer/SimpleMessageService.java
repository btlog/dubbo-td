package com.tiandee.dubbo.business.mq.producer;

import com.google.gson.Gson;
import com.tiandee.dubbo.api.mq.CreateOrderEvent;
import com.tiandee.dubbo.business.service.OrderService;
import com.tiandee.dubbo.common.mq.ExtRocketMQTemplateInfo;
import com.tiandee.dubbo.common.mq.MqProducer;
import com.tiandee.dubbo.common.mq.SendMessage;
import com.tiandee.dubbo.common.mq.SendMode;
import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.spring.core.RocketMQLocalTransactionListener;
import org.apache.rocketmq.spring.core.RocketMQLocalTransactionState;
import org.springframework.beans.factory.SmartInitializingSingleton;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.Message;
import org.springframework.stereotype.Component;

import java.nio.charset.StandardCharsets;
import java.util.concurrent.atomic.AtomicLong;

/**
 * @author wangjifa
 * @version 1.0
 * @description
 * @createtime 2023/9/5 6:01 PM
 * @updatetime
 */
@Component
@Slf4j
public class SimpleMessageService implements SmartInitializingSingleton{

    AtomicLong atomicLong = new AtomicLong(System.currentTimeMillis());

    @Autowired
    private MqProducer mqProducer;

    @Autowired
    private OrderService orderService;
    private static final Gson GSON = new Gson();

    public void send() {
        CreateOrderEvent createOrderEvent = new CreateOrderEvent();
        createOrderEvent.setAmount(988L);
        createOrderEvent.setName("订单");
        createOrderEvent.setOrderId(atomicLong.getAndIncrement()+"");
        createOrderEvent.setTopicName("order_topic");
        createOrderEvent.setBusinessKeyId(createOrderEvent.getOrderId());
        createOrderEvent.setBusinessTagId("createOrderEvent");
        mqProducer.execute(SendMessage.builder().message(createOrderEvent).build());
    }


    public void createTx(){
        CreateOrderEvent createOrderEvent = new CreateOrderEvent();
        createOrderEvent.setAmount(83L);
        createOrderEvent.setName("男装");
        createOrderEvent.setOrderId(atomicLong.getAndIncrement()+"");
        createOrderEvent.setTopicName("order_topic");
        createOrderEvent.setBusinessKeyId(createOrderEvent.getOrderId());
        createOrderEvent.setBusinessTagId("createOrderEvent");


        mqProducer.execute(SendMessage.builder().message(createOrderEvent).sendMode(SendMode.TX).templateInfo(templateInfo).build());

    }

    public static ExtRocketMQTemplateInfo templateInfo;

    @Override
    public void afterSingletonsInstantiated() {
        templateInfo = new ExtRocketMQTemplateInfo();
        templateInfo.setBeanName("extCreateOrderMqTemplate");
        templateInfo.setGroup("ext-createOrder-group");
        templateInfo.setRocketMQLocalTransactionListener(new RocketMQLocalTransactionListener() {
            @Override
            public RocketMQLocalTransactionState executeLocalTransaction(Message msg, Object arg) {
                CreateOrderEvent createOrderEvent = null;
                try {
                    createOrderEvent = getBusinessMessage(msg);
                    orderService.create(createOrderEvent);
                    Thread.sleep(5*1000L);
                    log.info("-----事务UNKNOWN---{}",createOrderEvent);
                    return RocketMQLocalTransactionState.UNKNOWN;
//                    return RocketMQLocalTransactionState.COMMIT;
                }catch (Exception e){
                    log.error("-----创建RocketMQ下单事务消息失败, ---"+createOrderEvent.getGid(),e);
                    return RocketMQLocalTransactionState.ROLLBACK;
                }
            }

            @Override
            public RocketMQLocalTransactionState checkLocalTransaction(Message msg) {
                CreateOrderEvent createOrderEvent = null;
                try {
                    createOrderEvent = getBusinessMessage(msg);
                    log.info("回查:{}",createOrderEvent);
                    Thread.sleep(5*1000L);
                    return RocketMQLocalTransactionState.COMMIT;
                }catch (Exception e){
                    log.error("-----回查异常 ---"+createOrderEvent.getGid(),e);
                    return RocketMQLocalTransactionState.UNKNOWN;
                }
            }
        });
        log.info(">>>>>>>>>>>>>extCreateOrderMqTemplate完成初始化<<<<<<<<<<<<<<<");
    }

    private CreateOrderEvent getBusinessMessage(Message msg){
        String jsonString = new String((byte[]) msg.getPayload(), StandardCharsets.UTF_8);
        return GSON.fromJson(jsonString,CreateOrderEvent.class);
    }

}
