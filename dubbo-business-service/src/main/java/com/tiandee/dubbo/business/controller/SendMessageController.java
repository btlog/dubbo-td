package com.tiandee.dubbo.business.controller;

import com.tiandee.dubbo.business.mq.producer.SimpleMessageService;
import com.tiandee.dubbo.common.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author wangjifa
 * @version 1.0
 * @description
 * @createtime 2023/9/7 4:20 PM
 * @updatetime
 */
@RestController
public class SendMessageController {

    @Autowired
    private SimpleMessageService simpleMessageService;

    @GetMapping("send")
    public Result send(){
        simpleMessageService.send();
        return new Result();
    }

    @GetMapping("tx")
    public Result tx(){
        simpleMessageService.createTx();
        return new Result();
    }
}
