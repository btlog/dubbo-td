package com.tiandee.dubbo.business.proxy;

import com.tiandee.dubbo.api.OrderApi;
import com.tiandee.dubbo.api.dto.CreateOrderRequest;
import com.tiandee.dubbo.api.dto.OrderDTO;
import com.tiandee.dubbo.common.Result;
import com.tiandee.dubbo.common.ex.BusinessException;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.DubboReference;
import org.springframework.stereotype.Component;

/**
 * @author wangjifa
 * @version 1.0
 * @description
 * @createtime 2023/8/31 1:58 PM
 * @updatetime
 */
@Component
@Slf4j
public class OrderProxy {

    @DubboReference
    private OrderApi orderApi;

    public OrderDTO createOrder(String userId, String commodityCode, int orderCount){

        CreateOrderRequest createOrderRequest = new CreateOrderRequest();
        createOrderRequest.setCommodityCode(commodityCode);
        createOrderRequest.setUserId(userId);
        createOrderRequest.setCount(orderCount);
        Result<OrderDTO> result = orderApi.create(createOrderRequest);
        if(result.isSuccess()){
            return result.getData();
        }
        throw new BusinessException(result.getCode(),result.getMessage());
    }

}
