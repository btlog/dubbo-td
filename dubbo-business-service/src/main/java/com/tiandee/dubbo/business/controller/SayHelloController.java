package com.tiandee.dubbo.business.controller;

import com.tiandee.dubbo.business.proxy.AsyncSayHelloProxy;
import com.tiandee.dubbo.common.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author wangjifa
 * @version 1.0
 * @description
 * @createtime 2023/9/4 5:32 PM
 * @updatetime
 */
@RestController
public class SayHelloController {

    @Autowired
    private AsyncSayHelloProxy asyncSayHelloProxy;


    @GetMapping("sayHello")
    public Result sayHello(@RequestParam  String name){
        Result result = new Result();
        asyncSayHelloProxy.sayHello(name);
        return result;
    }

    @GetMapping("sayHello2")
    public Result sayHello2(@RequestParam  String name){
        Result result = new Result();
        asyncSayHelloProxy.sayHello2(name);
        return result;
    }



}
