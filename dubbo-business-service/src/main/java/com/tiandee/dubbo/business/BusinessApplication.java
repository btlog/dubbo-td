package com.tiandee.dubbo.business;

import org.apache.dubbo.config.spring.context.annotation.EnableDubbo;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author wangjifa
 * @version 1.0
 * @description
 * @createtime 2023/8/31 1:43 PM
 * @updatetime
 */
@SpringBootApplication
@EnableDubbo
public class BusinessApplication {
    public static void main(String[] args) {
        SpringApplication.run(BusinessApplication.class, args);
    }
}
