package com.tiandee.dubbo.business.mq.consumer;

import com.tiandee.dubbo.api.mq.CreateOrderEvent;
import com.tiandee.dubbo.common.mq.MqConsumer;
import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.springframework.stereotype.Component;

/**
 * @author wangjifa
 * @version 1.0
 * @description
 * @createtime 2023/9/7 5:55 PM
 * @updatetime
 */
@Slf4j
@Component
@RocketMQMessageListener(
        topic = "order_topic",
        consumerGroup = "consumer-business-group",
        selectorExpression = "createOrderEvent",
        consumeThreadNumber = 2
)
public class SimpleMessageListener extends MqConsumer<CreateOrderEvent> implements RocketMQListener<CreateOrderEvent> {
    @Override
    protected String consumerName() {
        return "consumer-business-group";
    }
    @Override
    public void onMessage(CreateOrderEvent message) {
        super.dispatchMessage(message);
    }

    @Override
    protected void handleMessage(CreateOrderEvent message) throws Exception {
//        if(message.getRetryTimes()<2){
//            throw new BusinessException("我出异常了！！！");
//        }
        log.info("我在处理数据 {}",message);
    }

    @Override
    protected void overMaxRetryTimesMessage(CreateOrderEvent message) {

    }

    @Override
    protected boolean isRetry() {
        return true;
    }

    @Override
    protected boolean isThrowException() {
        return false;
    }


}
