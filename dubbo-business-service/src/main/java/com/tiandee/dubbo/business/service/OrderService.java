package com.tiandee.dubbo.business.service;

import com.tiandee.dubbo.api.mq.CreateOrderEvent;
import com.tiandee.dubbo.common.ex.Exceptions;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.concurrent.atomic.AtomicLong;

/**
 * @author wangjifa
 * @version 1.0
 * @description
 * @createtime 2023/9/8 3:01 PM
 * @updatetime
 */
@Slf4j
@Service
public class OrderService {

   AtomicLong atomicLong = new AtomicLong();

    public void create(CreateOrderEvent createOrderEvent){
        log.info("创建订单中.....{}",createOrderEvent);
        if(atomicLong.incrementAndGet()%2==0){
            log.info("模拟失败.....");
            Exceptions.rethrow("创建订单失败");
        }

    }

}
