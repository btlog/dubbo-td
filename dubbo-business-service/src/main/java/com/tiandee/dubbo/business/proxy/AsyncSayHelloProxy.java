package com.tiandee.dubbo.business.proxy;

import com.tiandee.dubbo.api.AsyncSayHelloApi;
import com.tiandee.dubbo.api.dto.SayHelloRequest;
import com.tiandee.dubbo.common.Result;
import org.apache.dubbo.config.annotation.DubboReference;
import org.apache.dubbo.rpc.RpcContext;
import org.springframework.stereotype.Component;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

/**
 * @author wangjifa
 * @version 1.0
 * @description
 * @createtime 2023/9/4 5:19 PM
 * @updatetime
 */
@Component
public class AsyncSayHelloProxy {

    @DubboReference(async=true)
    private AsyncSayHelloApi asyncSayHelloApi;

    @DubboReference
    private AsyncSayHelloApi asyncSayHelloApi2;

    public void sayHello(String name){

        SayHelloRequest sayHelloRequest = new SayHelloRequest();
        sayHelloRequest.setName(name);
        Result<String> result = asyncSayHelloApi.sayHello(sayHelloRequest);
        System.out.println("sayHello "+result);

        CompletableFuture<Result<String>> helloFuture = RpcContext.getServiceContext().getCompletableFuture();

        helloFuture.whenComplete((retValue, exception) -> {
            if (exception == null) {
                System.out.println("hello "+retValue.getData());
            } else {
                exception.printStackTrace();
            }
        });
        System.out.println("美好的一天");

    }


    public void sayHello2(String name){

        SayHelloRequest sayHelloRequest = new SayHelloRequest();
        sayHelloRequest.setName(name);


        CompletableFuture<Result<String>> future = RpcContext.getServiceContext().asyncCall(
                ()->{
                    Result<String> r = asyncSayHelloApi2.sayHello(sayHelloRequest);
                    System.out.println("sayHello "+r);
                    return r;
                }
        );

        try {
            System.out.println("hello "+future.get().getData());
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }


    }




}
