package com.tiandee.dubbo.api;

import com.tiandee.dubbo.api.dto.SayHelloRequest;
import com.tiandee.dubbo.common.Result;

/**
 * @author wangjifa
 * @version 1.0
 * @description
 * @createtime 2023/9/4 4:28 PM
 * @updatetime
 */
public interface AsyncSayHelloApi {

    Result<String> sayHello(SayHelloRequest sayHelloRequest);
}
