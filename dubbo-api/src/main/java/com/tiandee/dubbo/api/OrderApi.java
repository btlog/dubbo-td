package com.tiandee.dubbo.api;

import com.tiandee.dubbo.api.dto.CreateOrderRequest;
import com.tiandee.dubbo.api.dto.OrderDTO;
import com.tiandee.dubbo.common.Result;

/**
 * @author wangjifa
 * @version 1.0
 * @description
 * @createtime 2023/8/30 4:58 PM
 * @updatetime
 */
public interface OrderApi {

    Result<OrderDTO> create(CreateOrderRequest createOrderRequest);
}
