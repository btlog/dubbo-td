package com.tiandee.dubbo.api.dto;

import com.tiandee.dubbo.common.BaseRequest;

import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;

/**
 * @author wangjifa
 * @version 1.0
 * @description
 * @createtime 2023/8/30 4:57 PM
 * @updatetime
 */
public class CreateOrderRequest extends BaseRequest{

    private static final long serialVersionUID = -3842844815582212998L;
    @NotNull
    private String userId;
    @NotNull
    private String commodityCode;
    @Max(value=5,message = "最大值为5")
    private Integer count;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getCommodityCode() {
        return commodityCode;
    }

    public void setCommodityCode(String commodityCode) {
        this.commodityCode = commodityCode;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }
}
