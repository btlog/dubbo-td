package com.tiandee.dubbo.api.dto;

import com.tiandee.dubbo.common.BaseRequest;

import javax.validation.constraints.NotNull;

/**
 * @author wangjifa
 * @version 1.0
 * @description
 * @createtime 2023/9/4 4:29 PM
 * @updatetime
 */
public class SayHelloRequest extends BaseRequest {

    @NotNull
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
