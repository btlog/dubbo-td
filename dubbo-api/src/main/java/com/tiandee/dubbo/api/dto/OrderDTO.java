package com.tiandee.dubbo.api.dto;

import com.tiandee.dubbo.common.BaseDTO;

import java.io.Serializable;

/**
 * @author wangjifa
 * @version 1.0
 * @description
 * @createtime 2023/8/30 4:52 PM
 * @updatetime
 */
public class OrderDTO extends BaseDTO implements Serializable{

    private static final long serialVersionUID = 8285632529819180112L;

    private Long id;
    private String userId;
    private String commodityCode;
    private Integer count;
    private Integer money;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getCommodityCode() {
        return commodityCode;
    }

    public void setCommodityCode(String commodityCode) {
        this.commodityCode = commodityCode;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public Integer getMoney() {
        return money;
    }

    public void setMoney(Integer money) {
        this.money = money;
    }
}
