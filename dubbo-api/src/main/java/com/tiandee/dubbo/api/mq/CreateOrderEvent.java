package com.tiandee.dubbo.api.mq;

import com.tiandee.dubbo.common.mq.event.BaseEvent;

/**
 * @author wangjifa
 * @version 1.0
 * @description
 * @createtime 2023/9/5 6:14 PM
 * @updatetime
 */

public class CreateOrderEvent extends BaseEvent {

    private String orderId;
    private String name;
    private Long amount;

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getAmount() {
        return amount;
    }

    public void setAmount(Long amount) {
        this.amount = amount;
    }
}
